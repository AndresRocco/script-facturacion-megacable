import os
from openpyxl import Workbook
from openpyxl import load_workbook

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def copy_sheets(wb_source, wb_dest):
    print(f"{bcolors.HEADER}\nCopying sheets from workbook...{bcolors.ENDC}")


    for sheet_iterator in wb_source.sheetnames:
        working_sheet = wb_source[sheet_iterator]
        wb_dest.create_sheet(title=str(sheet_iterator))
        dest_sheet = wb_dest[str(sheet_iterator)]

        print("Copying {}...".format(sheet_iterator), end="")

        max_num_rows = working_sheet.max_row
        max_num_cols = working_sheet.max_column

        for i in range(1, max_num_rows + 1):
            for j in range(1, max_num_cols + 1):
                c = working_sheet.cell(row = i, column = j)

                dest_sheet.cell(row = i, column = j).value = c.value

        print (f"{bcolors.OKGREEN} Done.{bcolors.ENDC}")



def main():
    wb = Workbook()
    wb2 = load_workbook(filename='{}/Data/Colonias_Tarifas.xlsx'.format(os.getcwd()))


    wb3 = load_workbook(filename='{}/Data/ConcatenatedData.xlsx'.format(os.getcwd()))


    copy_sheets(wb3, wb)
    copy_sheets(wb2, wb)

    del wb["Sheet"] # Removes worksheet in index[0] which is created by default

    wb2.close()
    wb3.close()
    wb.save(filename='{}/Data/ConcatenatedData.xlsx'.format(os.getcwd()))

if __name__ == "__main__":
    main()
