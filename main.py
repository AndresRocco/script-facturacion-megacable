#!/usr/bin/python
import os
import subprocess

from helpers import *
import copy2

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def main():

    print(f"{bcolors.HEADER}\nNaming columns and formatting... {bcolors.ENDC}", end="")
    reader()
    print(f"{bcolors.OKGREEN} Done. {bcolors.ENDC}")

    print("Please, open Excel/WPS, sort 'NewData.xlsx' and save as 'SortedData.xlsx', then press Return.")
    input()

    while True:
        if os.path.isfile('{}/Data/SortedData.xlsx'.format(os.getcwd())):
            concatenate()
            break
        else:
            print(f"{bcolors.WARNING}Error: 'SortedData.xlsx' does not exist. Retry and press Return.{bcolors.ENDC}")
            input()

    copy2.main()
    format_data()


if __name__ ==  "__main__":
    main()
