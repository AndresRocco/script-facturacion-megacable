import os
import pandas
import sys
import subprocess
from openpyxl import load_workbook
from openpyxl.formula.translate import Translator

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# Copies every .csv in a given folder (searches for '/Data')
# into a single, new .xlxs file
def copy():

    if os.path.exists('{}/Data'.format(os.getcwd())):
        os.chdir('{}/Data'.format(os.getcwd()))

        # This below is the bash command to copy
        # every .csv into a new one
        subprocess.run('cat *.csv > Data.csv', shell=True)

    print("Finished copying. Data copied into 'Data.csv'")
    print("El directorio en el que se trabajó es:\n{}".format(os.getcwd()))


def reader():
    if os.path.isfile('{}/Data/Data.csv'.format(os.getcwd())):
        print("'Data.csv' found. Formatting... ")
    else:
        print(f"{bcolors.FAIL}'Data.csv' does not exist.{bcolors.ENDC}")
        sys.exit(1)

    # Lee el primer archivo original 'Data.csv'
    fp = pandas.read_csv('{}/Data/Data.csv'.format(os.getcwd()),
                         encoding='ISO-8859-1', low_memory=False, header=None)

#    Elimina columnas inecesarias, almacenando las necesarias
    keep_col = [14, 20, 16, 21, 22, 24, 25, 26, 27, 28, 30, 39, 29, 72]
    new_fp = fp[keep_col]

    # Escribe el nuevo archivo 'Data2.csv'
    # El cual ya no incluye las columnas inecesarias
    new_fp.to_csv('{}/Data/Data2.csv'.format(os.getcwd()), index=None, header=None, encoding='ISO-8859-1')

    # Repite el proceso, Lee 'Data2.csv', asigna nombres a columnas
    fp2 = pandas.read_csv('{}/Data/Data2.csv'.format(os.getcwd()), encoding = 'latin1', low_memory=False, names=['Sistema', 'Colonia', 'Tipo de Trabajo', 'P/A', 'ctr0', 'ctr1', 'ctr2', 'Folio', 'Cliente', 'Fecha REG', 'Fecha ATE', 'cda0', 'cda1', 'Total'])

    new_fp2 = fp2

#    Escribe el nuevo archivo 'NewData.xlsx', con nombres de columnas
    new_fp2.to_excel('{}/Data/NewData.xlsx'.format(os.getcwd()), index=None, encoding='ISO-8859-1')

def concatenate():
    pandas.options.mode.chained_assignment = None

    if os.path.isfile('{}/Data/SortedData.xlsx'.format(os.getcwd())):
        print("'SortedData.xlsx' found. Formatting... ")
    else:
        print("'SortedData.xlsx' does not exist.")
        sys.exit(1)

    fp = pandas.read_excel('{}/Data/SortedData.xlsx'.format(os.getcwd()))

    # Elimina duplicados, manteniendo la primera ocurrencia
    t_fp = fp.drop_duplicates(subset=['Folio'], keep='first', inplace=False)


    # Crea las nuevas columnas y las llena copiando los
    # Contenidos de las columnas necesarias
    t_fp['Contrato'] = t_fp['ctr0'].map(str) + t_fp['ctr1'].map(str) + t_fp['ctr2'].map(str)
    t_fp['Cuadrilla'] = t_fp['cda0'].map(str) + t_fp['cda1'].map(str)



    # Mantiene y ordena las columnas necesarias y elimina el resto
    keep_col = ['Sistema', 'Colonia', 'Tipo de Trabajo', 'P/A', 'Contrato', 'Folio', 'Cliente', 'Fecha REG', 'Fecha ATE', 'Cuadrilla', 'Total']

    concatenated_fp = t_fp[keep_col]

    concatenated_fp.to_excel('{}/Data/ConcatenatedData.xlsx'.format(os.getcwd()), index=False)


def format_data():
    wb = load_workbook(filename='{}/Data/ConcatenatedData.xlsx'.format(os.getcwd()))

    sheet = wb.active

    print(wb.sheetnames)

    sheet.insert_cols(5, amount=2)
    sheet.insert_cols(3, amount=1)
    sheet.insert_cols(5, amount=1)
    sheet['C1'].value = 'Aereo/Subterraneo'
    sheet['E1'].value = 'Combo/Sencillo'
    sheet['G1'].value = 'Trabajo a Pagar'
    sheet['H1'].value = 'Pagar'

    formula = "=IF(I2=I3,\"Combo\", \"Sencillo\")"

    sheet['E2'] = formula
    print(len(sheet['A']))

    max_row_num = sheet.max_row

    for row_num in range(2, max_row_num+1):
        sheet['E{}'.format(row_num)] = Translator(formula, origin='E2').translate_formula('E{}'.format(row_num))


    formula = "=CONCAT(D2,E2,F2)"
    sheet['G2'] = formula

    for row_num in range(2, max_row_num + 1):
        sheet['G{}'.format(row_num)] = Translator(formula, origin='G2').translate_formula('G{}'.format(row_num))

    formula = "=IF(OR(EXACT(A2,\"Morelia\"),EXACT(A2,\"Tarimbaro\")),VLOOKUP(B2,Colonias!$A$5:$B$564,2,FALSE),IF(OR(EXACT(B2,\"CENTRO\"),EXACT(B2,\"CENTRO (ACU)\"),EXACT(B2,\"CENTRO / INDEPENDENCIA\")),\"SUBTERRANEO\",\"AEREO\"))"

    sheet['C2'] = formula

    for row_num in range(2, max_row_num+1):
        sheet['C{}'.format(row_num)] = Translator(formula, origin='C2').translate_formula('C{}'.format(row_num))

    formula = "=IF(EXACT(C2,\"AEREO\"),VLOOKUP(G2,\'Tarifas Aereas\'!$A$3:$C$53,3,FALSE),IF(EXACT(C2,\"SUBTERRANEO\"),VLOOKUP(G2,\'Tarifas Subterraneas\'!$A$3:$C$53,3,FALSE)))"

    sheet['H2'] = formula

    for row_num in range(2, max_row_num + 1):
        sheet['H{}'.format(row_num)] = Translator(formula, origin='H2').translate_formula('H{}'.format(row_num))

    wb.save(filename='{}/Data/CompleteData.xlsx'.format(os.getcwd()))
